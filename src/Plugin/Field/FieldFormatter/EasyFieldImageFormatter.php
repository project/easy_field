<?php

namespace Drupal\easy_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'image' formatter.
 *
 * @FieldFormatter(
 *   id = "easy_field_image_formatter",
 *   module = "easy_field",
 *   label = @Translation("Image (with Easy Field data)"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class EasyFieldImageFormatter extends ImageFormatter {
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $easy_elements = array();
        $item_elements = array();
        foreach($items as $delta => $item){
            $item_values = $item->getValue();
            $item_elements[$delta]['target_id'] = $item_values['target_id'];
            $item_elements[$delta]['width'] = $item_values['width'];
            $item_elements[$delta]['height'] = $item_values['height'];
            $item_elements[$delta]['title'] = (isset($item_values['title'])) ? $item_values['title'] : '';
            $item_elements[$delta]['alt'] = (isset($item_values['alt'])) ? $item_values['alt'] : '';
        }

        $elements = parent::viewElements($items, $langcode);
        $files = $this->getEntitiesToView($items, $langcode);

        $image_style_setting = $this->getSetting('image_style');
        $img_style = ImageStyle::load($image_style_setting);

        foreach ($files as $delta => $file) {
            $image_uri = $file->getFileUri();
            $original_url = Url::fromUri(file_create_url($image_uri))->toString();
            $url = (!is_null($img_style)) ? $img_style->buildUrl($image_uri) : $original_url;
            $image_info = $this->image_get_info($url, $image_uri);
            $easy_elements[$delta]['entity_id'] = $item_elements[$delta]['target_id'];
            $easy_elements[$delta]['uri'] = $image_uri;
            $easy_elements[$delta]['url'] = $url;
            $easy_elements[$delta]['title'] = $item_elements[$delta]['title'];
            $easy_elements[$delta]['alt'] = $item_elements[$delta]['alt'];
            $easy_elements[$delta]['width'] = $image_info['width'];
            $easy_elements[$delta]['height'] = $image_info['height'];
            $easy_elements[$delta]['weight'] = $image_info['weight'];
            $easy_elements[$delta]['mime_type'] = $file->getMimeType();
            $easy_elements[$delta]['mime_type'] = $file->getMimeType();
            $easy_elements[$delta]['image_style'] = $image_style_setting;
            $easy_elements[$delta]['original']['url'] = $original_url;
            $easy_elements[$delta]['original']['width'] = $item_elements[$delta]['width'];
            $easy_elements[$delta]['original']['height'] = $item_elements[$delta]['height'];
            $easy_elements[$delta]['original']['weight'] = $file->getSize();
        }
        $elements['easyfield'] = $easy_elements;
        return $elements;
    }

    private function image_get_info($url, $uri){
        $dimension = getimagesize($url);
        $details['width'] = $dimension[0];
        $details['height'] = $dimension[1];
        return $details;
    }

}
